﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shops.Models;
using Newtonsoft.Json;

namespace Shops.Controllers
{
    public class ScheduleController : Controller
    {
        private ShopsEntities db = new ShopsEntities();
        private const string ALL = "All";

        //
        // GET: /Product/
        public ActionResult Index(string channelFilter, int? maxPrice)
        {
            ScheduleJsonProvider provider = new ScheduleJsonProvider();
            String json = provider.GetBroadcasts();
            var schedules = JsonConvert.DeserializeObject<List<Broadcast>>(json);
            ScheduleViewModel VM = new ScheduleViewModel();

            List<String> list = (from s in schedules select s.channel).Distinct().ToList();
            list = list.OrderBy(x => x).ToList();
            list.Insert(0, ALL);

            if ((channelFilter != null) && (channelFilter != ALL))
            {
                schedules = schedules.Where(s => s.channel == channelFilter).ToList();
            }

            ViewBag.ChannelList = list;
            ViewBag.ChannelFilter = channelFilter;

            //ViewBag.ShopFilter = shopFilter;
            //            var products = db.products.Include(p => p.shop);
            //            ProductViewModel VM = new ProductViewModel();

            //schedules = schedules.OrderBy(s => s.Start);
            VM.schedules = schedules; //.ToList();
            return View(VM);
        }



        //TODO
        public ActionResult GetProducts(string shopFilter, int? maxPrice)
        {
            JsonResult result;
            try
            {
                var products = db.products.Include(p => p.shop);

                if ((shopFilter != null) && (shopFilter != ALL))
                {
                    products = products.Where(p => p.shop.name == shopFilter);
                }
                if ((maxPrice != null))
                {
                    products = products.Where(p => p.price <= maxPrice);
                }

                result = Json(products.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                String errorMessage = ex.Message;
                if (ex.InnerException != null) errorMessage = ex.InnerException.Message.Substring(0, Math.Max(ex.InnerException.Message.Length, 20));
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return result;
        }


        //
        // GET: /Product/Details/5
        public ActionResult Details(int id = 0)
        {
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }


        //
        // GET: /Product/Create
        [Authorize(Users = "administrator")]
        public ActionResult Create()
        {
            ViewBag.shop_id = new SelectList(db.shops, "id", "name");
            return View();
        }






        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}