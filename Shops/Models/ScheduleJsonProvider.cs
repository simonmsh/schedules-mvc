﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;

namespace Shops.Models
{
    public class ScheduleJsonProvider
    {
        public ScheduleJsonProvider()
        {
            System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }


        public string GetBroadcasts()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync("https://sgurr-tv-listings-4.azurewebsites.net/service").Result;
            //string answer = "[{\"start\":\"2019 - 11 - 30 04:25\",\"channel\":\"3SAT\",\"yr\":\"2018\",\"title\":\"Vereinsheim Schwabing -Bühnensport mit Constanze Lindner\",\"epTitle\":\"\",\"desc\":\"UnterhaltungGute Laune ist garantiert, wenn Constanze Lindner im Vereinsheim Schwabing in München die besten Newcomer und Etablierten aus den Bereichen Stand-up - Comedy, Kabarett und Musik präsentiert. In typischer Wirtshaus - Atmosphäre lädt Constanze Lindner zum außergewöhnlichen Kleinkunstabend ein.Diesmal sind zu Gast: Simon Pearce, Duo Luna - tic, 5 / 8erl in Ehr'n, Sebastian Richartz und Das Ding ausm Sumpf. Als Sohn einer bekannten bayerischen Volksschauspielerin und eines Gastronomen aus Nigeria hatte Simon Pearce eine spannende Kindheit. Seinen bunten Hintergrund verarbeitet er in enorm lustigen Comedy-Nummern, bei denen man nie genau weiß, ob sie wirklich erlebt oder komplett erfunden sind. Außerdem ist Simon Pearce als Schauspieler und Synchronsprecher aktiv. Duo Luna-tic: geniales 'Klavierakrobatikliederkabarett' aus der Schweiz. Olli und Claire alias Judith Bach und Stéfanie Lang verzücken mit leisen und lauten Chansons voller Herz und Schmerz. 5/8erl in Ehr'n haben schon längst ihr eigenes Genre kreiert, das sie selbst als 'Wiener Soul' bezeichnen.Mit Jazz, Blues, einem Hauch Wienerlied und einer Riesenportion Schmäh erobern sie ihr Publikum. Ihr aktuelles Album heißt 'Der Duft der Männer'.Sebastian Richartz bezeichnet sich selbst als 'Rohdiamant der deutschen Comedy-Szene'.Seine Texte sind allerdings geschliffen. Unerbittlich sucht er mit seiner eher ausgefallenen Sicht auf die Welt den Humor an Stellen, an denen sonst niemand zu suchen wagt - irgendwo zwischen Alltag, Politik und Popkultur. Das Ding ausm Sumpf: Augsburg ist eigentlich nicht für seine Sümpfe bekannt. Dennoch tauchen da manchmal Dinge auf, die ein bisschen unheimlich sind -unheimlich gut.Das Ding ausm Sumpf ist eine Band, die der Oberpfälzer Dr. Stefan Mühlbauer um sich versammelt hat. 2016 hat sie ihr erstes Album, 'Raumzeit', vorgelegt: acht Songs prallvoll mit dichtem deutschen Sprechgesang.Bürger from the hell -das ist ein nicht ganz ernst gemeinter Spitzname für den ausgezeichneten Musiker Norbert Bürger.Er hat seine wahre Berufung, nämlich die kleinste Showband der Welt zu sein, gefunden. So heizt er dem Publikum auf seine schräge Art ordentlich ein.Abgerundet wird der Abend mit einem Lateinkurs der besonderen Art, wenn sich der Vereinsheim - Schankkellner Björn Puscha persönlich um das klassische Bildungsniveau des Publikums bemüht.\",\"typ\":\" \"},{ \"start\":\"2019-11-30 04:29\",\"channel\":\"Disney XD Norway\",\"yr\":\"\",\"title\":\"Lab Rats\",\"epTitle\":\"\",\"desc\":\"Leo finner ut at han skal være en student, ikke en mentor.seriesseries\",\"typ\":\"S\"},{ \"start\":\"2019-11-30 05:30\",\"channel\":\"BBC News 24\",\"yr\":\"\",\"title\":\"The Travel Show\",\"epTitle\":\"\",\"desc\":\"Join the team on their journey of discovery as they explore new destinations around the globe and uncover hidden sides to some of the world's favourite holiday hotspots.\",\"typ\":\"S\"},{ \"start\":\"2019-11-30 06:00\",\"channel\":\"BBC News 24\",\"yr\":\"\",\"title\":\"Breakfast\",\"epTitle\":\"\",\"desc\":\"A round-up of national and international news, plus sports reports, weather forecasts and arts and entertainment features. Including NewsWatch at 7.45.\",\"typ\":\"S\"}]";
            string answer = response.Content.ReadAsStringAsync().Result;
            return answer;
        }

    }

}