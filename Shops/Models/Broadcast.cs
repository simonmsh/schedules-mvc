﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shops.Models
{
    public class Broadcast
    {
        public DateTime start { get; set; }
        public string channel { get; set; }
        public int? yr { get; set; }
        public string title { get; set; }
        public string epTitle { get; set; }
        public string desc { get; set; }
        public string typ { get; set; }
    }
}